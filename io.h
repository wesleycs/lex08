#ifndef IO_H
#define IO_H

void copyFile(char * inFile, char * outFile);
int countLines(char * inFile);
int charCount(char * inFile);

#endif

// Io.h is declaring the functions that are assumedly in the Io.c file
// said functions are copyFile  and countFile which were utilized in 
// main
